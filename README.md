# Jenkins_JDK_17_Docker

This repository is help to run a Jenkins in docker environment with inside docker and JDK17.
For cases where you may want to perform a pipeline job in Jenkins that requires building a docker image from a git repo pulled from GitHub, installing Docker inside of the container can help with this.

## Useful to know:
-  This guide created for linux env.

# Useful links:
- Install docker local: https://docs.docker.com/engine/install/
- Install Jenkins: https://faun.pub/how-to-install-docker-in-jenkins-container-4c49ba40b373
- Build Java application: https://www.section.io/engineering-education/building-a-java-application-with-jenkins-in-docker/
- Windows help: https://tomgregory.com/running-docker-in-docker-on-windows/


## Getting started

1. `$ sudo chmod 666 /var/run/docker.sock`
   - Change permissions on “docker.sock”
   - If you are on **Windows** you can read more about this step here: https://tomgregory.com/running-docker-in-docker-on-windows/


2. `$ sudo docker-compose up -d `
   - Make sure that the .env ports are free
   - Create and start containers


4. `$ sudo docker ps -a `
   - You have to find the Jenkins container(jenkins-jdk17-docker:latest) id to start a interactive shell inside the container.


5. `$ sudo docker exec -it --user root <container id> bash`
   - This command run an interactive shell on the Jenkins container that has been created, so we can login to docker-hub.


6. `$ docker-login (inside the continer)`
   - You can log in to you DockerHub account inside the container.


7. `$ cat /var/jenkins_home/secrets/initialAdminPassword (inside the container)`
   - This will print the administration password 
   

8. Create job for your Java application
   - You can access jenkins in ${CONSOLE_PORT}
   - You have to creae a Dockerfile in your java project
   - https://www.section.io/engineering-education/building-a-java-application-with-jenkins-in-docker/